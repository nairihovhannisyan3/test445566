import uuid, os, shutil
import time
import telebot
import requests
from telebot import apihelper, time, logging
# import Adafruit_DHT


logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

TG_PROXY = 'https://103.241.156.250:8080'
apihelper.proxy = {'http' : TG_PROXY}
bot = telebot.TeleBot('config.token')

API_TOKEN = "1292521216:AAFG73G8bmWVEsSVM8JRqEyGNuZsfz23Cig"

bot = telebot.TeleBot(API_TOKEN)

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Hello, Welcome to our bot.")

@bot.message_handler(commands=['help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "With this bot you can short your long urls. ❤️")
    bot.send_message(message.chat.id, "Use @short <url> to short.")
    bot.send_message(message.chat.id, "To host front end website send me html text, file, or zip with index.html to host it.")
    bot.send_message(message.chat.id, "Use @temp view temperature and humidity.")

def save_message(message):
    my_uuid = uuid.uuid1()
    os.mkdir(f'public/{my_uuid}')
    file = open(f'public/{my_uuid}/index.html', 'a')
    text = message.text
    print(my_uuid, text)
    file.write(str(text))
    file.close()
    os.system(f'git pull')
    git_push(my_uuid);
    url = f'https://nairihovhannisyan3.gitlab.io/test445566/{my_uuid}'
    bot.reply_to(message, url)

@bot.message_handler(content_types=['text'])
def url_shortener(message):
    command = message.text[:7]
    # if "@temp" in message.text:
    #     sensor = Adafruit_DHT.DHT11
    #     gpio = 17
    #     humidity, temperature = Adafruit_DHT.read(sensor, gpio)
    #     if humidity is not None and temperature is not None:
    #         bot.send_message(message.chat.id, 'Temperature={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
    #     else:
    #         bot.send_message(message.chat.id, 'Failed to get reading. Try again!')
    #     time.sleep(1)
    # elif command == "@short ":
    #     link_to_short = message.text.replace(command, '')
    #     post_url = 'http://api.xn--y9aua5byc.xn--y9a3aq/urls'
    #     data = {'url': link_to_short}
    #     x = requests.post(post_url, data = data)
    #     print(x.text[:7])
    #     short_key = x.text[:7]
    #     short_url = "նա.հայ/?id=" + short_key
    #     text = "Your short url is there. Thank you!!! ❤️❤️\n"
    #     bot.reply_to(message, text+short_url)
    # else:
    save_message(message)

def git_push(uuid):
    os.system(f'git rm -rf --cached public/{uuid} .')
    os.system(f'git add .')
    os.system(f'git commit -m "adding {uuid}"')
    os.system(f'git push')

def download_file(uuid, url, ext):
    r = requests.get(url, stream=True)
    with open(f'./{uuid}.{ext}', 'wb') as f:
        print(f"Downloading {uuid}")
        total_length = r.headers.get('content-length')
        print(total_length)
        f.write(r.content)

@bot.message_handler(content_types=['document'])
def save_html(message):
    doc_type = message.document.mime_type
    my_uuid = uuid.uuid1()
    file_id = message.document.file_id
    file_name = message.document.file_name
    file = bot.get_file(file_id)
    file_path = file.file_path
    if doc_type == "text/html":
        url = f'https://api.telegram.org/file/bot{API_TOKEN}/{file_path}'
        ext = 'html'
        download_file(file_name, url, ext)
        os.mkdir(f'public/{my_uuid}')
        shutil.move(file_name, f'public/{my_uuid}/index.html')
        os.system(f'git pull')
        git_push(my_uuid)
        url = f'https://nairihovhannisyan3.gitlab.io/test445566/{my_uuid}'
        bot.reply_to(message, url)
    elif doc_type == "application/zip":
        file_full_name = file_name.split(".")[0]
        url = f'https://api.telegram.org/file/bot{API_TOKEN}/{file_path}'
        ext = 'zip'
        download_file(my_uuid, url, ext)
        os.system(f"unzip {my_uuid}.{ext}")
        print(file_full_name)
        os.remove(f'{my_uuid}.zip')
        shutil.move(file_full_name, f'public/{my_uuid}')
        time.sleep(2)
        os.system(f'git pull')
        git_push(my_uuid)
        url = f'https://nairihovhannisyan3.gitlab.io/test445566/{my_uuid}'
        bot.reply_to(message, url)
    else:
        print(f"A none html file. It is {doc_type}")

bot.infinity_polling()
